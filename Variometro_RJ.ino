
/*
Librería ms5611 con correccion a -11ºC 
https://github.com/gronat/MS5611



*/



#include <MS5611.h>

MS5611 baro;
int32_t presion_raw;
int32_t temperatura_raw;

void setup() {
  // Start barometer
  baro = MS5611();
  baro.begin();
  // Start serial (UART)
  Serial.begin(9600);
  delay(2);
}

void loop() {
  // Read pressure
  presion_raw = baro.getPressure();
  Serial.println(presion_raw);
  delay(100);
  
}